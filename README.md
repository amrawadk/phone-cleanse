# Phone Cleanse

This is a quick project where I looked at analyzing the applications on my Xiaomi phone, and de-bloat it. I also wanted to understand which applications were taking the most space.

## Steps

1. I used the command `adb shell dumpsys diskstats` to get the app details, and with some `re` magic, parse the results into a `Package` data class.

2. With the help of [VSCode's interactive mode](https://code.visualstudio.com/docs/python/jupyter-support-py), I was able to quickly get a table of all packages, that I can filter sort and change, here's an example of what that looks like:

![image](imgs/packages_data_viewer.png)


3. I used [`hurry.filesize`](https://pypi.org/project/hurry.filesize/) to parse the byte file sizes into human friendly strings.

4. After identifying which packages I wanted to remove, I had to options:

a) uninstall the app using: `adb shell pm uninstall -k --user 0 <package-name>`

but that sometimes failed with `Failure [-1000]` as Xiaomi didn't allow uninstalling some system apps, I'd then have to resort to the second option.

b) disable the app with: `adb shell pm disable-user <package-name>`, which hides it from the UI.

## Learnings

- It was interesting diving more into adb, I love the idea of having more control over my phone from a shell, will definetly love exploring all the posibilities more in the future.
- VSCode's interactive mode in Python is quite powerful, and gives me the best of both worlds:
    - and IDE where I'm most productive with quick short cuts, and linting tools
    - a jupyter notebook where I can quickly iterate on code snippets
Great to have that in the tool box as well :D
- I love deleting apps on my phone and removing bloatware to reduce the clutter, and help me focus my time.

## Gitlab CLI

I also took this as a chance to explore [Gitlab's CLI](https://python-gitlab.readthedocs.io/en/stable/cli.html) 

- Install the Gitlab CLI
```
pipx install 'python-gitlab[autocompletion]
```
- Add my configurations in `~/.python-gitlab.cfg`:

```ini
[global]
default = gitlab
ssl_verify = true 
timeout = 5

[gitlab]
url = https://gitlab.com
private_token = <redacted>
```
- Create the project using 
```bash
gitlab project create --name phone-cleanse
```
- Set the remote origin
```bash
git remote add origin $(gitlab -v -o json project get --id 25573008 | jq '.ssh_url_to_repo' -r)
```

# %%

import re
from dataclasses import dataclass
import subprocess
from typing import List
import hurry.filesize as hfs


@dataclass
class Package:
    name: str
    size: int
    data_size: int
    cache_size: int

    def __repr__(self) -> str:
        fields = dict(
            name=self.name,
            size=hfs.size(self.size),
            data_size=hfs.size(self.data_size),
            cache_size=hfs.size(self.cache_size),
        )
        return f"Package({fields}))"


def get_packages() -> List[Package]:
    output = subprocess.check_output("adb shell dumpsys diskstats".split())

    package_names = eval(re.search(r"Package Names: (.*)", output.decode()).group(1))
    app_sizes = eval(re.search(r"App Sizes: (.*)", output.decode()).group(1))
    app_data_sizes = eval(re.search(r"App Data Sizes: (.*)", output.decode()).group(1))
    cache_sizes = eval(re.search(r"Cache Sizes: (.*)", output.decode()).group(1))


    return [
        Package(name=name, size=size, data_size=data_size, cache_size=cache_size)
        for name, size, data_size, cache_size in zip(
            package_names, app_sizes, app_data_sizes, cache_sizes
        )
    ]

packages = get_packages()
sorted(packages, key=lambda p: p.cache_size, reverse=True)[:4]

# %%
